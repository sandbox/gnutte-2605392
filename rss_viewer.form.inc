<?php

/**
 * @file
 * Form file.
 */

/**
 * The feed's block configuration form.
 */
function rss_viewer_block_form($form, &$form_state, $module = NULL, $delta = NULL) {
  $block = db_select('block', 'b')->fields('b', array('bid'))->condition('module', $module)->condition('delta', $delta)->execute()->fetch();

  if (!isset($form_state['values'])) {
    $feeds = db_select('block_feed', 'f')->fields('f')->condition('bid', $block->bid)->execute()->fetchAll();

    $form_state['values'] = array(
      'enable' => FALSE,
      'module' => $module,
      'delta' => $delta,
    );

    if (!empty($feeds)) {
      $feed = array_shift($feeds);
      $form_state['values']['enable'] = TRUE;
      $form_state['values']['config'] = array(
        'url' => $feed->feed,
        "mode" => $feed->mode,
        "number" => $feed->size,
      );
    }
  }

  $values = $form_state['values'];

  $form['module'] = array(
    '#type' => 'hidden',
    '#value' => $values['module'],
  );

  $form['delta'] = array(
    '#type' => 'hidden',
    '#value' => $values['delta'],
  );

  $form['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Link RSS feed'),
    '#default_value' => $values['enable'],
    '#ajax' => array(
      'callback' => 'rss_viewer_block_form_ajax_callback',
      'wrapper' => 'rss-viewer-block-form-config',
      'method' => 'replace',
    ),
  );

  $form['config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuration'),
    '#prefix' => "<div id='rss-viewer-block-form-config'>",
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  if ($values['enable']) {
    $form['config']['feed'] = array(
      '#type' => 'textfield',
      '#title' => t('RSS feed'),
      '#default_value' => $values['config']['url'],
    );
    $form['config']['mode'] = array(
      '#type' => 'select',
      '#title' => t('View mode'),
      '#options' => array(
        'list' => t('List'),
        'teaser' => t('Teaser'),
        'full' => t('Full'),
      ),
      '#default_value' => $values['config']['mode'],
    );
    $form['config']['number'] = array(
      '#type' => 'textfield',
      '#title' => t('View count'),
      '#default_value' => $values['config']['number'],
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * The feed's configuration form ajax callback.
 */
function rss_viewer_block_form_ajax_callback($form, $form_state) {
  return $form['config'];
}

/**
 * The feed's block configuration form submit.
 */
function rss_viewer_block_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];

  $block = db_select('block', 'b')->fields('b', array('bid'))->condition('module', $values['module'])->condition('delta', $values['delta'])->execute()->fetch();
  if (!is_object($block)) {
    return;
  }

  db_delete('block_feed')->condition('bid', $block->bid)->execute();

  if ($values['enable']) {
    db_insert('block_feed')
        ->fields(array(
          'bid' => $block->bid,
          'feed' => $values['config']['feed'],
          'mode' => $values['config']['mode'],
          'size' => $values['config']['number'],
        ))
        ->execute();
  }

  drupal_set_message(t("The configuration has been saved."));

  $form_state['redirect'] = 'admin/structure/block';
}
