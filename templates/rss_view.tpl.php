<?php
/**
 * @file
 * Default simple rss feed template.
 *
 * Variables :
 *   - feed : the rss feed link
 *   - items : feeds element
 *     - title
 *     - link
 *   - mode : the feed's display mode (list, teaser or full)
 *   - size : the number of feed's item we must show.
 */
?>

<div class="feed-view">
  <?php for ($i = 0; $i < $size; $i++): ?>
    <div class="feed-item">
      <a target="blank" href="<?php print $items[$i]['link']; ?>"><?php print $items[$i]['title']; ?></a>
    </div>
  <?php endfor; ?>
</div>
