CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The RSS viewer module allow you to have a look on external RSS feeds 
on your site without having to import data.
For each block you create, you can associate it to a RSS feed. 

REQUIREMENTS
------------
This module requires the following modules:
* Aggregator 

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Create a block in Administration >> Structure >> Blocks

   - Enable RSS association by clicking on "Link RSS feed" in the FEED form
   - Set feed configuration in "RSS feed", "View mode" and "View count" fields

 * Customize the display of feed by rewriting the template 
   rss-view__[view_mode].tpl.php in your theme's templates folder

MAINTAINERS
-----------

Current maintainers:
 * Grégory Nutte (gnut) - https://drupal.org/user/2409626
